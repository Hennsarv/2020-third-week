﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NorthWindAken
{
    public partial class Form1 : Form
    {
        DataBaseEntities db = new DataBaseEntities("NWEntities");

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
                var products = db.Products
                    .Select(x => new { x.ProductID, x.ProductName, x.UnitPrice, x.Category.CategoryName})
                    .ToList();
                this.dataGridView1.DataSource = products;
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
                var categories = db.Categories
                    .Select(x => new { x.CategoryID, x.CategoryName, x.Description, Tooteid = x.Products.Count() })
                    .ToList();
                this.dataGridView1.DataSource = categories;
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listBox1.DataSource =
                db.Categories
                .OrderBy(x => x.CategoryID)
                .Select(x => x.CategoryName)
                .ToList();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Text = (checkBox1.Checked ?
                "Näitan kõiki tooteid" :
                "Näitan ainult kehtivaid tooteid")
                + " kategooriast " + listBox1.SelectedItem
                ;

            var tooted = db.Products
                .Where(x => this.checkBox1.Checked || !x.Discontinued)
                .Where(x => x.Category.CategoryName == this.listBox1.SelectedItem.ToString())
                .Select(x => new { x.ProductID, x.ProductName, x.UnitPrice, x.Discontinued })
               .ToList();
            this.dataGridView1.DataSource = tooted;
        }
    }
}
