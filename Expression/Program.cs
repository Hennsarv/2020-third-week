﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace MidagiMuud
{
    static class E
    {
        public static string ToProper(this string s)
            => s == "" ? "" :
            s.Substring(0, 1).ToUpper() + s.Substring(1).ToLower();
    }

    class Program
    {
        static void Main(string[] args)
        {
            Func<int, int> f = x => x * x;
            Console.WriteLine(f);

            Expression<Func<int,int>> e = x => x * x + 100;
            Console.WriteLine(e);

            // f = e.Compile();
            Console.WriteLine(e.Compile()(16));

            using(DataBaseEntities db = new DataBaseEntities())
            {
                foreach(var p in db.Products
                    .AsEnumerable()
                    .Where(x => x.ProductName.ToProper() == "Chai")
                    .Select(x => new { x.ProductName, x.UnitPrice })
                    )
                    Console.WriteLine(p);
            }


        }
    }
}
