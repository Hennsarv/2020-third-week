﻿namespace ViimaneAken
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.LoeTooted = new System.Windows.Forms.Button();
            this.nimeOsa = new System.Windows.Forms.TextBox();
            this.otsiToode = new System.Windows.Forms.Button();
            this.alumineHind = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ylemineHind = new System.Windows.Forms.TextBox();
            this.nameSort = new System.Windows.Forms.Button();
            this.hindSort = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(918, 426);
            this.dataGridView1.TabIndex = 0;
            // 
            // LoeTooted
            // 
            this.LoeTooted.Location = new System.Drawing.Point(951, 17);
            this.LoeTooted.Name = "LoeTooted";
            this.LoeTooted.Size = new System.Drawing.Size(148, 23);
            this.LoeTooted.TabIndex = 1;
            this.LoeTooted.Text = "Loe Tooted";
            this.LoeTooted.UseVisualStyleBackColor = true;
            this.LoeTooted.Click += new System.EventHandler(this.LoeTooted_Click);
            // 
            // nimeOsa
            // 
            this.nimeOsa.Location = new System.Drawing.Point(951, 58);
            this.nimeOsa.Name = "nimeOsa";
            this.nimeOsa.Size = new System.Drawing.Size(148, 22);
            this.nimeOsa.TabIndex = 2;
            // 
            // otsiToode
            // 
            this.otsiToode.Location = new System.Drawing.Point(951, 176);
            this.otsiToode.Name = "otsiToode";
            this.otsiToode.Size = new System.Drawing.Size(148, 23);
            this.otsiToode.TabIndex = 3;
            this.otsiToode.Text = "Otsi Toodet";
            this.otsiToode.UseVisualStyleBackColor = true;
            this.otsiToode.Click += new System.EventHandler(this.otsiToode_Click);
            // 
            // alumineHind
            // 
            this.alumineHind.Location = new System.Drawing.Point(999, 104);
            this.alumineHind.Name = "alumineHind";
            this.alumineHind.Size = new System.Drawing.Size(100, 22);
            this.alumineHind.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(951, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = ">=";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(951, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "<=";
            // 
            // ylemineHind
            // 
            this.ylemineHind.Location = new System.Drawing.Point(999, 132);
            this.ylemineHind.Name = "ylemineHind";
            this.ylemineHind.Size = new System.Drawing.Size(100, 22);
            this.ylemineHind.TabIndex = 6;
            // 
            // nameSort
            // 
            this.nameSort.Location = new System.Drawing.Point(954, 217);
            this.nameSort.Name = "nameSort";
            this.nameSort.Size = new System.Drawing.Size(145, 23);
            this.nameSort.TabIndex = 8;
            this.nameSort.Text = "Nime järgi";
            this.nameSort.UseVisualStyleBackColor = true;
            this.nameSort.Click += new System.EventHandler(this.nameSort_Click);
            // 
            // hindSort
            // 
            this.hindSort.Location = new System.Drawing.Point(954, 256);
            this.hindSort.Name = "hindSort";
            this.hindSort.Size = new System.Drawing.Size(145, 23);
            this.hindSort.TabIndex = 9;
            this.hindSort.Text = "Hinna järgi";
            this.hindSort.UseVisualStyleBackColor = true;
            this.hindSort.Click += new System.EventHandler(this.hindSort_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1111, 450);
            this.Controls.Add(this.hindSort);
            this.Controls.Add(this.nameSort);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ylemineHind);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.alumineHind);
            this.Controls.Add(this.otsiToode);
            this.Controls.Add(this.nimeOsa);
            this.Controls.Add(this.LoeTooted);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button LoeTooted;
        private System.Windows.Forms.TextBox nimeOsa;
        private System.Windows.Forms.Button otsiToode;
        private System.Windows.Forms.TextBox alumineHind;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ylemineHind;
        private System.Windows.Forms.Button nameSort;
        private System.Windows.Forms.Button hindSort;
    }
}

