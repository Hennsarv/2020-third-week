﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViimaneAken
{
    public partial class Form1 : Form
    {
        List<Product> products = new List<Product>();
        string sorting = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void LoeTooted_Click(object sender, EventArgs e)
        {
            using(NorthwindEntities db = new NorthwindEntities())
            {
                
                this.dataGridView1.DataSource
                    = products = db.Products.ToList();
                sorting = "";
                nameSort.Enabled =
                hindSort.Enabled = true;
            }
        }

        private void otsiToode_Click(object sender, EventArgs e)
        {
            decimal alumine = decimal.TryParse(alumineHind.Text, out decimal da) ? da : -1;
            decimal ylemine = decimal.TryParse(ylemineHind.Text, out decimal dy) ? dy : -1;

            using (NorthwindEntities db = new NorthwindEntities())
            {
                this.dataGridView1.DataSource = products
                    = db.Products
                    .Where(x => nimeOsa.Text == "" || x.ProductName.Contains(nimeOsa.Text))
                    .Where(x => alumine < 0 || x.UnitPrice >= alumine)
                    .Where(x => ylemine < 0 || x.UnitPrice <= ylemine)
                    .ToList();
                sorting = "";
                nameSort.Enabled =
                hindSort.Enabled = true;

            }
        }

       

        private void nameSort_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource
                = sorting == "name" ?
                products.OrderByDescending(x => x.ProductName).ToList():
                products.OrderBy(x => x.ProductName).ToList();
            sorting = sorting == "name" ? "" : "name";
        }

        private void hindSort_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource
                = sorting == "price" ?
                products.OrderByDescending(x => x.UnitPrice).ToList() :
                products.OrderBy(x => x.UnitPrice).ToList();
            sorting = sorting == "price" ? "" : "price";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            hindSort.Enabled =
            nameSort.Enabled = false;
        }
    }
}
