﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KonsooligaAken
{
    class Program
    {
        public static string nimi = "";

        static void Main(string[] args)
        {
            System.Threading.Thread t = new System.Threading.Thread(Teine);
            t.Start();
            System.Threading.Thread e = new System.Threading.Thread(Esimene);
            e.Start();
            System.Threading.Thread.Sleep(20000);
        }

        static void Esimene()
        {
            while (true)
            {
                Console.Write("Kes sa oled: ");
                nimi = Console.ReadLine();
                Console.WriteLine($"Tere {nimi}!"); 
            }
            
        }
        static void Teine()
        {
            //Console.WriteLine("Tahaks akent");
            Form1 f = new Form1();
            f.ShowDialog();

        }
    }
}
