﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SpordipäevaAken
{
    public static class Program
    {
        public static List<ProtokolliRida> Protokoll;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Protokoll =
            File.ReadAllLines(@"..\..\spordipäeva protokoll.txt")
                //.Skip(1)
                .Select(x => x.Replace(", ", ",").Split(','))
                .Select(x => new ProtokolliRida
                {
                    Nimi = x[0],
                    Distants = int.Parse(x[1]),
                    Aeg = int.Parse(x[2])
                })
                .ToList();


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }

    public class ProtokolliRida
    {
        public string Nimi { get; set; }
        public int Distants { get; set; }
        public int Aeg  { get; set; }

        public string Kiirus => (Distants * 1.0 / Aeg).ToString("F2");
    }
}
