﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpordipäevaAken
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private bool started = false;
        private DateTime stardiaeg;
        private DateTime lõpuaeg;

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "") label4.Text = "puudub nimi";
            if (textBox2.Text == "") label4.Text = "puudub distants";
            else if (textBox1.Text != "")
            {
                started = true;
                stardiaeg = DateTime.Now;
                this.timer1.Interval = 10;
                this.timer1.Start();
                this.timer1.Tick += timer1_Tick;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.label3.Text = (DateTime.Now - stardiaeg).TotalSeconds.ToString("f2");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            lõpuaeg = DateTime.Now;
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Program.Protokoll.Add(
                new ProtokolliRida
                {
                    Nimi = this.textBox1.Text,
                    Distants = int.Parse(this.textBox2.Text),
                    Aeg = (int)((lõpuaeg-stardiaeg).TotalSeconds+0.5)
                }
                );
            ;
        }
    }
}
