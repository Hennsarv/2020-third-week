﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace DBConsool
{
    
    class Program
    {
        static void Main(string[] args)
        {
 
            //
            // Connection - minu ja DB vaheline ühendus
            // Command - minu küsimus andmebaasile
            // Datareader - tema vastus minu küsimusele
            string connectionString =   "Data Source=valiitsql.database.windows.net;" +
                                        "Initial Catalog=Northwind;" +
                                        "User ID=Student;" +
                                        "Password=Pa$$w0rd";
            using (var conn = new SqlConnection(connectionString))
            {
                
                conn.Open();
                string küsimus = "select productname, unitprice from products";
                using (var comm = new SqlCommand(küsimus, conn))
                {
                    
                    var R = comm.ExecuteReader();

                    while (R.Read())
                    {
                        Console.WriteLine($"{R["productname"]} hinnaga {R["unitprice"]}");
                    } 
                } // tehakse command objekt puhtaks
                // sellega lõppes esimene näide andmebaasiga tööst
                // me teeme väikse pausi, mille käigus, kõik
                //      * korrastavad oma mõtted peale suuremat ehmatus
                //      * täidavad oma kohvitassi

            } // tehakse selle connectioniga puhastus


        }
    }
}
