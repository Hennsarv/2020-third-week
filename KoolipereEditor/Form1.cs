﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace KoolipereEditor
{
    
    public partial class Form1 : Form
    {
        List<Inimene> Loetud = null;
        string fileName = "";
        bool õpsid = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void loeÕpilased_Click(object sender, EventArgs e)
        {
            õpsid = false;
            this.fileName = @"..\..\Õpilased.txt";
            this.Loetud = File.ReadAllLines(fileName)
                .Select(x => x.Replace(", ", ",").Split(','))
                .Select(x => new Inimene { IK = x[0], Nimi = x[1], Klass = x[2] })
                .ToList();
            this.andmetabel.Visible = true;
            this.andmetabel.DataSource = Loetud;
            this.andmetabel.Columns["Aine"].Visible = false;
            this.salvestaNupp.Visible = true;
        }

        private void loeÕpetajad_Click(object sender, EventArgs e)
        {
            õpsid = true;
            this.fileName = @"..\..\Õpetajad.txt";
            this.Loetud = File.ReadAllLines(fileName)
                .Select(x => (x+",,").Replace(", ", ",").Split(','))
                .Select(x => new Inimene { IK = x[0], Nimi = x[1], Klass = x[3], Aine = x[2] })
                .ToList();
            this.andmetabel.Visible = true;
            this.andmetabel.DataSource = Loetud;
            this.andmetabel.Columns["Aine"].Visible = true;
            this.salvestaNupp.Visible = true;
        }

        private void salvestaNupp_Click(object sender, EventArgs e)
        {
            // aidake mõelda, kuidas ma tean, keda salvestada
            File.WriteAllLines(fileName,
                Loetud
                .Select(x => õpsid ?
                        $"{x.IK},{x.Nimi},{x.Aine},{x.Klass}" :   // õpetajarida
                        $"{x.IK},{x.Nimi},{x.Klass}"     // õpilasterida
                ))
                ;

        }

       
    }
}
