﻿namespace KoolipereEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.andmetabel = new System.Windows.Forms.DataGridView();
            this.loeÕpilased = new System.Windows.Forms.Button();
            this.loeÕpetajad = new System.Windows.Forms.Button();
            this.salvestaNupp = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.andmetabel)).BeginInit();
            this.SuspendLayout();
            // 
            // andmetabel
            // 
            this.andmetabel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.andmetabel.Location = new System.Drawing.Point(42, 110);
            this.andmetabel.Name = "andmetabel";
            this.andmetabel.RowHeadersWidth = 51;
            this.andmetabel.RowTemplate.Height = 24;
            this.andmetabel.Size = new System.Drawing.Size(720, 309);
            this.andmetabel.TabIndex = 0;
            this.andmetabel.Visible = false;
            // 
            // loeÕpilased
            // 
            this.loeÕpilased.Location = new System.Drawing.Point(42, 22);
            this.loeÕpilased.Name = "loeÕpilased";
            this.loeÕpilased.Size = new System.Drawing.Size(152, 23);
            this.loeÕpilased.TabIndex = 1;
            this.loeÕpilased.Text = "Loe Õpilased";
            this.loeÕpilased.UseVisualStyleBackColor = true;
            this.loeÕpilased.Click += new System.EventHandler(this.loeÕpilased_Click);
            // 
            // loeÕpetajad
            // 
            this.loeÕpetajad.Location = new System.Drawing.Point(42, 66);
            this.loeÕpetajad.Name = "loeÕpetajad";
            this.loeÕpetajad.Size = new System.Drawing.Size(152, 23);
            this.loeÕpetajad.TabIndex = 2;
            this.loeÕpetajad.Text = "Loe Õpetajad";
            this.loeÕpetajad.UseVisualStyleBackColor = true;
            this.loeÕpetajad.Click += new System.EventHandler(this.loeÕpetajad_Click);
            // 
            // salvestaNupp
            // 
            this.salvestaNupp.Location = new System.Drawing.Point(686, 21);
            this.salvestaNupp.Name = "salvestaNupp";
            this.salvestaNupp.Size = new System.Drawing.Size(75, 23);
            this.salvestaNupp.TabIndex = 3;
            this.salvestaNupp.Text = "Salvesta";
            this.salvestaNupp.UseVisualStyleBackColor = true;
            this.salvestaNupp.Visible = false;
            this.salvestaNupp.Click += new System.EventHandler(this.salvestaNupp_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.salvestaNupp);
            this.Controls.Add(this.loeÕpetajad);
            this.Controls.Add(this.loeÕpilased);
            this.Controls.Add(this.andmetabel);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.andmetabel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView andmetabel;
        private System.Windows.Forms.Button loeÕpilased;
        private System.Windows.Forms.Button loeÕpetajad;
        private System.Windows.Forms.Button salvestaNupp;
    }
}

