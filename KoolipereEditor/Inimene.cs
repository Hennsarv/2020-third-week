﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KoolipereEditor
{
    class Inimene
    {
        public string IK { get; set; }
        public string Nimi { get; set; }
        public string Aine { get; set; } // ainult õpetajatel
        public string Klass { get; set; }

    }
}
