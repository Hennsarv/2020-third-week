﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        string tehe = "";
        TextBox kumb = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void Number_Click(object sender, EventArgs e)
        {
            kumb.Text += ((Button)sender).Text;
            label2.Text = "";
        }

        public void Tehe_Click(object sender, EventArgs e)
        {
            tehe = ((Button)sender).Text;
            label1.Text = "valitud tehe on " + tehe;
            Vaheta(false);
            label2.Text = "";

            foreach(var x in this.Controls)
            {
                if(x is Button b)
                {
                    b.BackColor = b.Parent.BackColor;
                }
            }

            ((Button)sender).BackColor = Color.Red;
        }

        void Vaheta(bool b = true)
        {
            if(b)
            {
                kumb = textBox1;
                Esimene.Visible = true;
                Teine.Visible = false;
            } else
            {
                kumb = textBox2;
                Esimene.Visible = false;
                Teine.Visible = true;
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Vaheta();
            Clear_Click(sender, e);
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            Vaheta();
            label2.Text = "";

        }

        private void Arvuta_Click(object sender, EventArgs e)
        {
            try
            {
                // arvutamine
                int arv1 = int.Parse(textBox1.Text);
                int arv2 = int.Parse(textBox2.Text);
                int tulemus =
                    tehe == "+" ? arv1 + arv2 :
                    tehe == "-" ? arv1 - arv2 :
                    tehe == "*" ? arv1 * arv2 :
                    tehe == "/" ? arv1 / arv2 : 0;
                label2.Text = tulemus.ToString();
                label2.ForeColor = Color.Black;

            }
            catch (Exception err)
            {
                label2.Text = err.Message;
                label2.ForeColor = Color.Red;
            }
        }
    }
}
