﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisposablePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 1000000; i++)
            {
                using (Inimene henn = new Inimene { Nimi = "Henn" })
                {
                    // toimetab midagi

                }


            }        }
    }

    class Inimene : IDisposable
    {
        static int Inimesi = 0;

        public int Nr { get; private set; } = ++Inimesi;
        public string Nimi { get; set; }
        public int Vanus { get; set; }

        static Inimene()
        {
            Console.WriteLine("algab inimkond");
        }

        public Inimene() : this("tundmatu", 0) { }
        public Inimene(string Nimi, int vanus)
        {

            //(Nimi, Vanus) = (nimi, vanus);
            this.Nimi = Nimi;
            this.Vanus = vanus;
            Console.WriteLine($"tehti {this}");
        }

        ~Inimene()
        {
            Console.WriteLine($"{this} eemaldatakse mälust");
            Dispose(false);
        }

        
        public override string ToString()
            => $"{Nr}. Inimene {Nimi} vanusega {Vanus}";

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Inimene()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
             GC.SuppressFinalize(this);
        }
        #endregion
    }
}
