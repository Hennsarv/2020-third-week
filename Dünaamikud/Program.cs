﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dünaamikud
{
    class Bag : DynamicObject
    {
        dynamic bag;
        public Bag()
        {
            bag = new Dictionary<string, dynamic>();
        }
        public Bag(dynamic bag) => this.bag = bag;

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            switch (bag)
            {
                case IDictionary<string, object> d:
                    result = d.ContainsKey(binder.Name) ? d[binder.Name] : null;
                    return true;
                default:
                    result = bag[binder.Name];
                    return true;
            }

        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            bag[binder.Name] = value;
            return true;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            int arv = 7;
            string sõna = "MiskiSõna";
            

            object o = arv;
            
            Console.WriteLine(o);
            o = sõna;
           
            Console.WriteLine(o);

            dynamic d = arv;
            d++;
            Console.WriteLine(d);
            d = sõna;
            Console.WriteLine(d.GetType().Name) ;

            dynamic bbb = new Bag();
            bbb.Nimi = "Henn";
            bbb.Vanus = 64;
            bbb.Vanus++;
            Console.WriteLine(bbb.Vanus);

           


            Dictionary<string, dynamic> asjad = new Dictionary<string, dynamic>();
            asjad["esimene"] = 77;
            asjad["teine"] = "hobune";


            dynamic aaa = new Bag(asjad);
            Console.WriteLine(asjad["teine"]);
            Console.WriteLine(aaa.teine);

            aaa.Sünniaeg = DateTime.Now;
            Console.WriteLine(asjad["Sünniaeg"]);

            



        }
    }
}
