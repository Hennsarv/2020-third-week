﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisAsiOnEvent
{
    class Program
    {
        static void Juubelipidu(Inimene i)
        { 
            Console.WriteLine("teeme pidu"); 
        }

        static void SaabVanaks(Inimene x)
        {
            Console.WriteLine($"saab pidu - {x.Nimi} saab {x.Vanus} aastaseks");
        }

        static void Main(string[] args)
        {
            Inimene henn = new Inimene { Nimi = "Henn", Vanus=64 };

            henn.Juubel += SaabVanaks;            //x => Console.WriteLine($"saab pidu - {x.Nimi} saab {x.Vanus} aastaseks");
            henn.Juubel +=
                (Inimene x) =>
                {
                    Console.WriteLine($"peaks {x.Nimi} jaoks vist kingituse ostma");
                };
            henn.Pensioniiga += x => Console.WriteLine("kas sa lähed juba pensionile");

            henn.Juubel += Juubelipidu;
            henn.Juubel -= Juubelipidu;

            while(henn.Vanus < 120)
            {
                henn.Vanus++;
                if (henn.Vanus == 80) henn.Pensionile();
            }

        }
    }

    class Inimene
    {
        public event Action<Inimene> Pensioniiga;
        public event Action<Inimene> Juubel;

        bool kasOnPensionil = false;
        public void Pensionile() => this.kasOnPensionil = true;

        public string Nimi;
        private int vanus = 0;
        public int Vanus { get => vanus;
            set
            {

                if (value < vanus) throw new Exception("inimest nooremaks teha ei saa");
                vanus = value;
                if (vanus % 25 == 0)
                    if (Juubel != null) this.Juubel(this);
                if (vanus > 70 && !kasOnPensionil) Pensioniiga?.Invoke(this);
            }
        }
    }

}
