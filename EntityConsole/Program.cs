﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            using (NorthwindEntities db = new NorthwindEntities())
            {
                //db.Database.Log = Console.WriteLine;

                // siin on päring extensionitena
                var q1 = db.Products
                    .Where(x => x.UnitPrice < 10)
                    .OrderBy(x => x.UnitPrice)
                    .Select(x => new { x.ProductName, x.UnitPrice, x.UnitsInStock });

                // päring LINQ avaldisena
                var q2 = from x in db.Products
                         where x.UnitPrice < 10
                         orderby x.UnitPrice
                         select new { x.ProductName, x.UnitPrice, x.UnitsInStock };

                Console.WriteLine("\nSiin alles hakkame küsima\n");
                foreach (var x in q2) Console.WriteLine(x);

                var kala = db.Categories.Find(8);
                Console.WriteLine(kala.CategoryName);

                if (kala != null)
                {
                    kala.CategoryName = "Seatoit";
                    db.SaveChanges();
                }

            }
        }
    }
}
